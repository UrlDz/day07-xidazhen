package com.thoughtworks.springbootemployee.data;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;


public class DataRepository {
    private static List<Employee> employees = new ArrayList<>();
    private static AtomicLong atomicEmployeeId = new AtomicLong(0);
    private static List<Company> companies = new ArrayList<>();
    private static AtomicLong atomicCompanyId = new AtomicLong(0);


    public static List<Employee> generateEmployees() {
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(),"a",10,"male",1000.0,1L));
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(),"b",20,"male",2000.0,2L));
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(),"c",30,"male",4000.0,1L));
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(),"d",40,"male",3000.0,2L));
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(),"e",50,"male",5000.0,1L));
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(),"f",60,"male",7000.0,1L));
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(),"g",20,"male",6000.0,2L));
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(),"h",30,"male",4000.0,1L));
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(),"i",40,"male",5000.0,1L));
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(),"j",50,"male",3000.0,2L));
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(),"k",60,"male",2000.0,1L));
        employees.add(new Employee(atomicEmployeeId.incrementAndGet(), "l", 70, "male", 1000.0, 1L));
        return employees;
    }

    public static List<Company> generateCompanies() {
        companies.add(new Company(atomicCompanyId.incrementAndGet(),"OOCL"));
        companies.add(new Company(atomicCompanyId.incrementAndGet(),"CargoSmart"));
        companies.add(new Company(atomicCompanyId.incrementAndGet(),"TW"));
        return companies;
    }

    public static Company addCompany(Company company) {
        company.setId(atomicCompanyId.incrementAndGet());
        companies.add(company);
        return company;
    }

    public static boolean deleteCompany(Long id) {
        Company company = companies.stream()
                .filter(eachCompany -> Objects.equals(id, eachCompany.getId()))
                .findFirst().get();
        return companies.remove(company);
    }

}
