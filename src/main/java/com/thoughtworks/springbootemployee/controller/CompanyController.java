package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.data.DataRepository;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RequestMapping("/companies")
@RestController
public class CompanyController {

    private List<Company> companies = new ArrayList<>();
    private List<Employee> employees = new ArrayList<>();

    private void generateCompanies() {
        companies = DataRepository.generateCompanies();
    }

    private void generateEmployees() {
        employees = DataRepository.generateEmployees();
    }

    @GetMapping()
    public List<Company> listCompanies() {
        generateCompanies();
        return companies;
    }


    @GetMapping("/{companyId}")
    public List<Company> listCompanies(@PathVariable Long companyId) {
        generateCompanies();
        return companies.stream().filter(company -> Objects.equals(company.getId(), companyId)).collect(Collectors.toList());
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> listCompanyEmployees(@PathVariable Long companyId) {
        generateCompanies();
        generateEmployees();
        return employees.stream()
                .filter(employee -> Objects.equals(
                        employee.getCompanyId(),
                        companies.stream()
                                .filter(company -> Objects.equals(company.getId(), companyId))
                                .findFirst()
                                .get()
                                .getId()
                        )
                ).collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Company create(@RequestBody Company company) {
        return DataRepository.addCompany(company);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompaniesByPage(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        generateCompanies();
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    @PutMapping("/{id}")
    public Company updateCompanyName(@PathVariable Long id, @RequestBody Company company) {
        generateCompanies();
        Company updateCompany = companies.stream()
                .filter(eachCompany -> Objects.equals(id, eachCompany.getId()))
                .findFirst().get();
        updateCompany.setName(company.getName());
        return updateCompany;
    }

    @DeleteMapping("/{id}")
    public Boolean deleteCompany(@PathVariable Long id) {
        generateCompanies();
        return DataRepository.deleteCompany(id);
    }
}
