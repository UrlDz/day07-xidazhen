package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.data.DataRepository;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private List<Employee> employees = new ArrayList<>();

    private static AtomicLong atomicId = new AtomicLong(0);

    private void generateEmployees() {
        employees = DataRepository.generateEmployees();
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Employee create(@RequestBody Employee employee) {
        employee.setId(atomicId.incrementAndGet());
        employees.add(employee);
        return employee;
    }

    @GetMapping
    public List<Employee> getEmployees() {
        generateEmployees();
        return employees;
    }

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable Long id) {
        generateEmployees();
        return employees.stream()
                .filter(employee -> Objects.equals(id, employee.getId())).findFirst().get();
    }

    @GetMapping(params = "gender")
    public List<Employee> getEmployeesByGender(@PathParam("gender") String gender) {
        generateEmployees();
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getGender(), gender))
                .collect(Collectors.toList());
    }

    @PutMapping("/{id}")
    public Employee updateEmployeeAgeAndSalary(@PathVariable Long id, @RequestBody Employee employee) {
        generateEmployees();
        Employee updateEmployee = employees.stream()
                .filter(employee1 -> Objects.equals(id, employee1.getId()))
                .findFirst().get();
        updateEmployee.setAge(employee.getAge());
        updateEmployee.setSalary(employee.getSalary());
        return updateEmployee;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteEmployeeById(@PathVariable Long id) {
        generateEmployees();
        Employee deleteEmployee = employees.stream()
                .filter(everyEmployee -> Objects.equals(id, everyEmployee.getId()))
                .findFirst().get();
        employees.remove(deleteEmployee);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> getEmployeeByPage(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }
}
